package com.alexnae.kebabsmvc.base;

import java.time.LocalDate;

/**
 * Created by Alex Nae on 04/11/2021.
 **/
public class Pita extends Kebab {

    private double rodajasCebolla;

    public Pita() {
    }

    public Pita(String nombreLocal, String extras, String unidades, LocalDate fechaPedido, String tipoCarne, double rodajasCebolla, int salsa) {
        super(nombreLocal, extras, unidades, fechaPedido, tipoCarne, salsa);
        this.rodajasCebolla = rodajasCebolla;
    }

    public double getRodajasCebolla() {
        return rodajasCebolla;
    }

    public void setRodajasCebolla(double rodajasCebolla) {
        this.rodajasCebolla = rodajasCebolla;
    }

    @Override
    public String toString() {
        return "Pita{" + super.toString()+", "+
                "rodajasCebolla=" + rodajasCebolla +
                '}';
    }
}
