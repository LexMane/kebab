package com.alexnae.kebabsmvc.base;

import java.time.LocalDate;

/**
 * Created by Alex Nae on 04/11/2021.
 */
public class Durum extends Kebab {
    private int rodajasTomate;

    public Durum() {
    }

    public Durum(int rodajasTomate) {
        this.rodajasTomate = rodajasTomate;
    }

    public Durum(String nombreLocal, String extras, String unidades, LocalDate fechaPedido, String tipoCarne, int rodajasTomate, int salsa) {
        super(nombreLocal, extras, unidades, fechaPedido, tipoCarne, salsa);
        this.rodajasTomate = rodajasTomate;
    }

    public int getRodajasTomate() {
        return rodajasTomate;
    }

    public void setRodajasTomate(int rodajasTomate) {
        this.rodajasTomate = rodajasTomate;
    }

    @Override
    public String toString() {
        return "Durum{" + super.toString()+", "+
                "rodajasTomate=" + rodajasTomate +
                '}';
    }
}
