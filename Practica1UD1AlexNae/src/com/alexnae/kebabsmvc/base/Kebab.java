package com.alexnae.kebabsmvc.base;

import java.time.LocalDate;

/**
 * Created by Alex Nae on 04/11/2021.
 */
public abstract class Kebab {
    private String nombreLocal;
    private String extras;
    private String unidades;
    private LocalDate fechaPedido;
    private String tipoCarne;
    private int salsa;

    public Kebab() {

    }

    public Kebab(String nombreLocal, String extras, String unidades, LocalDate fechaPedido, String tipoCarne, int salsa) {
        this.nombreLocal = nombreLocal;
        this.extras = extras;
        this.unidades = unidades;
        this.fechaPedido = fechaPedido;
        this.tipoCarne = tipoCarne;
        this.salsa=salsa;
    }

    public String getNombreLocal() {
        return nombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getUnidades() {
        return unidades;
    }

    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }

    public LocalDate getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(LocalDate fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getTipoCarne() { return tipoCarne; }

    public void setTipoCarne(String tipoCarne) { this.tipoCarne = tipoCarne; }

    public int getSalsa() {
        return salsa;
    }

    public void setSalsa(int salsa) {
        this.salsa = salsa;
    }

    @Override
    public String toString() {
        return  "nombreLocal='" + nombreLocal + '\'' +
                ", extras='" + extras + '\'' +
                ", unidades='" + unidades + '\'' +
                ", fechaPedido=" + fechaPedido +
                ", tipoCarne='" + tipoCarne + '\'' +
                ", salsa= " + salsa ;
    }
}
