package com.alexnae.kebabsmvc;

import com.alexnae.kebabsmvc.gui.KebabControlador;
import com.alexnae.kebabsmvc.gui.KebabModelo;
import com.alexnae.kebabsmvc.gui.Ventana;

/**
 * Created by DAM on 28/10/2021.
 */
public class Principal {
    public static void main(String[] args) {
        Ventana vista= new Ventana();
        KebabModelo modelo=new KebabModelo();
        KebabControlador controlador = new KebabControlador(vista,modelo);
    }
}
