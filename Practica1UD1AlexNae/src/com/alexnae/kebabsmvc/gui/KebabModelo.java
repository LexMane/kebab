package com.alexnae.kebabsmvc.gui;

import com.alexnae.kebabsmvc.base.Durum;
import com.alexnae.kebabsmvc.base.Kebab;
import com.alexnae.kebabsmvc.base.Pita;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Alex Nae on 04/11/2021.
 **/
public class KebabModelo {
    private ArrayList<Kebab> listaKebabs;

    public KebabModelo() {
        listaKebabs = new ArrayList<Kebab>();
    }

    public ArrayList<Kebab> obtenerKebabs() {
        return listaKebabs;
    }

    public void altaDurum(String nombreLocal, String extras, String unidades,
                          LocalDate fechaPedido, String tipoCarne, int rodajasTomate, int salsa) {
        Durum nuevoDurum = new Durum(nombreLocal, extras, unidades, fechaPedido, tipoCarne, rodajasTomate, salsa);
        listaKebabs.add(nuevoDurum);
    }

    public void altaPita(String nombreLocal, String extras, String unidades,
                         LocalDate fechaPedido, String tipoCarne, double rodajasCebolla, int salsa) {
        Pita nuevaPita = new Pita(nombreLocal,extras,unidades,fechaPedido,tipoCarne,rodajasCebolla, salsa);
        listaKebabs.add(nuevaPita);
    }

    public boolean existeNombreLocal(String nombreLocal) {
        for (Kebab unKebab : listaKebabs) {
            if (unKebab.getNombreLocal().equals(nombreLocal)) {
                return true;
            }
        }
        return false;
    }

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //añado el nodo raiz - la primera etiqueta (contiene las demas)
        Element raiz = documento.createElement("Kebabs");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoKebab = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Kebab unKebab : listaKebabs) {
            //añado dentro de la etiqueta raiz Kebabs
            //en funcion del tipo kebab
            //durum o pita
            if (unKebab instanceof Durum) {
                nodoKebab = documento.createElement("Durum");
            } else {
                nodoKebab = documento.createElement("Pita");
            }
            raiz.appendChild(nodoKebab);

            //dentro de la etiqueta vehiculo
            //las subetiquetas (matricula, marca,...)

            nodoDatos=documento.createElement("Nombrelocal");
            nodoKebab.appendChild(nodoDatos);
            texto=documento.createTextNode(unKebab.getNombreLocal());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("Extras");
            nodoKebab.appendChild(nodoDatos);
            texto=documento.createTextNode(unKebab.getExtras());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("Unidades");
            nodoKebab.appendChild(nodoDatos);
            texto=documento.createTextNode(unKebab.getUnidades());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("Fechapedido");
            nodoKebab.appendChild(nodoDatos);
            texto=documento.createTextNode(unKebab.getFechaPedido().toString());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("Tipocarne");
            nodoKebab.appendChild(nodoDatos);
            texto=documento.createTextNode(unKebab.getTipoCarne());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("Salsa");
            nodoKebab.appendChild(nodoDatos);
            texto=documento.createTextNode(String.valueOf(unKebab.getSalsa()));
            nodoDatos.appendChild(texto);

            //como hay un dato que depende del tipo de vehiculo
            //tengo que controlar el tipo de objeto
            if (unKebab instanceof Durum) {
                nodoDatos=documento.createElement("Rodajastomate");
                nodoKebab.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Durum) unKebab).getRodajasTomate()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos=documento.createElement("Rodajascebolla");
                nodoKebab.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Pita) unKebab).getRodajasCebolla()));
                nodoDatos.appendChild(texto);
            }

            //guardo los datos en fichero
            Source source = new DOMSource(documento);
            Result resultado=new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source,resultado);

        }
    }

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaKebabs =new ArrayList<Kebab>();
        Durum nuevoDurum =null;
        Pita nuevaPita =null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        Document documento=builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i=0;i<listaElementos.getLength();i++) {
            Element nodoKebab = (Element) listaElementos.item(i);

            if (nodoKebab.getTagName().equals("Durum")) {
                nuevoDurum =new Durum();
                nuevoDurum.setNombreLocal(nodoKebab.getChildNodes().item(0).getTextContent());
                nuevoDurum.setExtras(nodoKebab.getChildNodes().item(1).getTextContent());
                nuevoDurum.setUnidades(nodoKebab.getChildNodes().item(2).getTextContent());
                nuevoDurum.setFechaPedido(LocalDate.parse(nodoKebab.getChildNodes().item(3).getTextContent()));
                nuevoDurum.setTipoCarne(nodoKebab.getChildNodes().item(4).getTextContent());
                nuevoDurum.setSalsa(Integer.parseInt(nodoKebab.getChildNodes().item(5).getTextContent()));
                nuevoDurum.setRodajasTomate(Integer.parseInt(nodoKebab.getChildNodes().item(6).getTextContent()));
                listaKebabs.add(nuevoDurum);
            }else {
                if (nodoKebab.getTagName().equals("Pita")) {
                    nuevaPita =new Pita();
                    nuevaPita.setNombreLocal(nodoKebab.getChildNodes().item(0).getTextContent());
                    nuevaPita.setExtras(nodoKebab.getChildNodes().item(1).getTextContent());
                    nuevaPita.setUnidades(nodoKebab.getChildNodes().item(2).getTextContent());
                    nuevaPita.setFechaPedido(LocalDate.parse(nodoKebab.getChildNodes().item(3).getTextContent()));
                    nuevaPita.setTipoCarne(nodoKebab.getChildNodes().item(4).getTextContent());
                    nuevaPita.setSalsa(Integer.parseInt(nodoKebab.getChildNodes().item(5).getTextContent()));
                    nuevaPita.setRodajasCebolla(Double.parseDouble(nodoKebab.getChildNodes().item(6).getTextContent()));
                    listaKebabs.add(nuevaPita);
                }
            }

        }

    }
}
