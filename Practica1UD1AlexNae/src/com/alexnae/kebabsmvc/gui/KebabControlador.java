package com.alexnae.kebabsmvc.gui;

import com.alexnae.kebabsmvc.base.Durum;
import com.alexnae.kebabsmvc.base.Kebab;
import com.alexnae.kebabsmvc.base.Pita;
import com.alexnae.kebabsmvc.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * Created by Alex Nae on 04/11/2021.
 **/
public class KebabControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private KebabModelo modelo;
    private File ultimaRutaExportada;

    public KebabControlador(Ventana vista, KebabModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("No existe el fichero de configuracion" + e.getMessage());
        }
        //añadimos listener
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("kebabs.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());

        configuracion.store(new PrintWriter("kebabs.conf"),
                "Datos configuracion kebabs");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios \n" +
                            "Nombre de Local \n Extras \n Unidades \n Fecha del Pedido \n Tipo de Carne \n"  +
                            vista.rodajasTxt.getText());
                    break;
                }

                if (modelo.existeNombreLocal(vista.nombreLocalTxt.getText())) {
                    Util.mensajeError("Ya existe un Local llamado así\n" +
                            vista.rodajasTxt.getText());
                    break;
                }
                if (vista.durumRadioButton.isSelected()) {
                    if (vista.polloRadioButton.isSelected()) {

                        modelo.altaDurum(vista.nombreLocalTxt.getText(),
                                vista.extrasTxt.getText(),
                                vista.unidadesTxt.getText(),
                                vista.fechaPedidoDPicker.getDate(),
                                vista.polloRadioButton.getText(),
                                Integer.parseInt(vista.rodajasTxt.getText()),
                                vista.Salsa.getValue());
                    }else{ modelo.altaDurum(vista.nombreLocalTxt.getText(),
                            vista.extrasTxt.getText(),
                            vista.unidadesTxt.getText(),
                            vista.fechaPedidoDPicker.getDate(),
                            vista.terneraRadioButton.getText(),
                            Integer.parseInt(vista.rodajasTxt.getText()),
                            vista.Salsa.getValue());

                    }



                } else { if (vista.polloRadioButton.isSelected()) {

                    modelo.altaPita(vista.nombreLocalTxt.getText(),
                            vista.extrasTxt.getText(),
                            vista.unidadesTxt.getText(),
                            vista.fechaPedidoDPicker.getDate(),
                            vista.polloRadioButton.getText(),
                            Double.parseDouble(vista.rodajasTxt.getText()),
                            vista.Salsa.getValue());
                }else{ modelo.altaPita(vista.nombreLocalTxt.getText(),
                        vista.extrasTxt.getText(),
                        vista.unidadesTxt.getText(),
                        vista.fechaPedidoDPicker.getDate(),
                        vista.terneraRadioButton.getText(),
                        Double.parseDouble(vista.rodajasTxt.getText()),
                        vista.Salsa.getValue()); }
                }

                limpiarCampos();
                refrescar();
                break;

            case "Importar":
                JFileChooser selectorFichero =
                        Util.crearSelectorFichero(ultimaRutaExportada,
                                "Archivos XML","xml");
                int opt=selectorFichero.showOpenDialog(null);
                if (opt==JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (SAXException e1) {
                        e1.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2=
                        Util.crearSelectorFichero(ultimaRutaExportada,"Archivos XML","xml");
                int opt2=selectorFichero2.showSaveDialog(null);
                if (opt2==JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (TransformerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;
            case "Pita":
                vista.rodajasLbl.setText("Rodajas de Cebolla");
                break;
            case "Durum":
                vista.rodajasLbl.setText("Rodajas de Tomate");
                break;
        }
    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        //solo ejecuto el codigo si el valor se está ajustando
        if (e.getValueIsAdjusting()) {
            Kebab kebabSeleccionado = (Kebab) vista.list1.getSelectedValue();
            vista.nombreLocalTxt.setText(kebabSeleccionado.getNombreLocal());
            vista.extrasTxt.setText(kebabSeleccionado.getExtras());
            vista.unidadesTxt.setText(kebabSeleccionado.getUnidades());
            vista.fechaPedidoDPicker.setDate(kebabSeleccionado.getFechaPedido());
            if (kebabSeleccionado.getTipoCarne().equals(vista.polloRadioButton.getText())){
                vista.polloRadioButton.doClick();
            }else {
                vista.terneraRadioButton.doClick();
            }



            if (kebabSeleccionado instanceof Durum) {
                vista.durumRadioButton.doClick();
                vista.rodajasTxt.setText(String.valueOf(((Durum) kebabSeleccionado).getRodajasTomate()));
            } else {
                vista.pitaRadioButton.doClick();
                vista.rodajasTxt.setText(String.valueOf(((Pita) kebabSeleccionado).getRodajasCebolla()));
            }
        }

    }

    //listener de los radioButton y botones
    private void addActionListener(ActionListener listener) {
        vista.durumRadioButton.addActionListener(listener);
        vista.pitaRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
    }

    //listener del frame
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    //listener de la lista
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    //limpiar campos
    private void limpiarCampos() {
        vista.rodajasTxt.setText(null);
        vista.extrasTxt.setText(null);
        vista.nombreLocalTxt.setText(null);
        vista.unidadesTxt.setText(null);
        vista.fechaPedidoDPicker.setText(null);
        vista.nombreLocalTxt.requestFocus();
    }

    //comprobar campos vacios
    private boolean hayCamposVacios() {
        if (vista.rodajasTxt.getText().isEmpty() ||
                vista.extrasTxt.getText().isEmpty() ||
                vista.nombreLocalTxt.getText().isEmpty() ||
                vista.unidadesTxt.getText().isEmpty() ||
                vista.fechaPedidoDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    //cargar datos en la lista
    public void refrescar() {
        vista.dlmKebab.clear();
        for (Kebab unKebab : modelo.obtenerKebabs()) {
            vista.dlmKebab.addElement(unKebab);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea cerrar la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarDatosConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);

            }
        }


    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }
}
