package com.alexnae.kebabsmvc.gui;

import com.alexnae.kebabsmvc.base.Kebab;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Created by Alex Nae on 04/11/2021.
 **/
public class Ventana {
    private JPanel panel1;
    public JRadioButton durumRadioButton;
    public JRadioButton pitaRadioButton;
    public JTextField nombreLocalTxt;
    public JTextField extrasTxt;
    public JTextField unidadesTxt;
    public JTextField rodajasTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public DatePicker fechaPedidoDPicker;
    public JLabel rodajasLbl;
    public JRadioButton polloRadioButton;
    public JRadioButton terneraRadioButton;
    public JSlider Salsa;

    public JFrame frame;

    public DefaultListModel<Kebab> dlmKebab;

    public Ventana() {
        frame = new JFrame("Kebab Amigo");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmKebab =new DefaultListModel<Kebab>();
        list1.setModel(dlmKebab);
    }


}
